package main

import "fmt"

func main() {
	sliceInt := []int{-8, 233, -1, 22222, -2232121, 20, -11, -2334, 656, 12, 78, -55, 0}
	sortedSlice := sortSlice(sliceInt)
	fmt.Println(sortedSlice)
}

func sortSlice(sliceInt []int) []int {
	sliceSorted := []int{}
	for _, v := range sliceInt {
		if len(sliceSorted) == 0 {
			sliceSorted = append(sliceSorted, v)
		} else {
			var pos int
			for i, sorted := range sliceSorted {
				if v < sorted {
					pos = i
					break
				}

				// pos = -1 to indicate appending
				if i == len(sliceSorted)-1 {
					pos = -1
				}
			}
			switch pos {
			case 0:
				sliceSorted = append([]int{v}, sliceSorted...)
			case -1:
				sliceSorted = append(sliceSorted, v)
			default:
				sliceBefore := []int{}
				sliceAfter := []int{}
				for i, v := range sliceSorted {
					if i < pos {
						sliceBefore = append(sliceBefore, v)
					} else {
						sliceAfter = append(sliceAfter, v)
					}
				}
				sliceSorted = append(sliceBefore, v)
				sliceSorted = append(sliceSorted, sliceAfter...)
			}
		}
	}

	return sliceSorted
}
