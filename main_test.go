package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSortSlice(t *testing.T) {
	testSlice := []int{-1, 8, 9, 0, -15, 2, 2, -654, 1900}
	expectedSlice := []int{-654, -15, -1, 0, 2, 2, 8, 9, 1900}
	sortedSlice := sortSlice(testSlice)
	assert.Equal(t, expectedSlice, sortedSlice)
}
